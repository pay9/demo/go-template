package main

import (
	"log"
	"net/http"
	"text/template"
)

type page struct {
	Author string
	Email  string
}

func home(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseGlob("template/*"))
	p := page{
		"เกษม อานนทวิลาศ",
		"mrtomyum@gmail.com",
	}
	t.ExecuteTemplate(w, "layout", p)
}

func main() {
	mux := http.NewServeMux()

	mux.HandleFunc("/", home)

	log.Println("Listening at http://localhost:80")
	http.ListenAndServe(":80", mux)
}
